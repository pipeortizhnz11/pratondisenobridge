public class Demo {
    public static void main(String[] args) {
        FiguraColor circuloRojo = new FiguraColor(new Circulo(), new Rojo());
        FiguraColor cuadradoAzul = new FiguraColor(new Cuadrado(), new Azul());

        circuloRojo.dibujar();
        cuadradoAzul.dibujar();
    }
}
