public class FiguraColor {
    private Figura figura;
    private Color color;

    public FiguraColor(Figura figura, Color color) {
        this.figura = figura;
        this.color = color;
    }

    public void dibujar() {
        figura.dibujar();
        color.pintar();
    }
}
